import React, { useEffect } from 'react';

export default function (props) {

    return <div className="flex flex-row">
        <div className="basis-1/4">
            {props.documentationId}
        </div>
        <div className="basis-3/4">Starter</div>
    </div>
}
