/** @type {import('tailwindcss').Config} */
module.exports = {
  important: true,
  content: [
    './assets/**/*.js',
    './templates/**/*.html.twig',
    './src/Form/*.php',
    './node_modules/tw-elements/dist/js/**/*.js',
    './assets/react/controllers/*.jsx'
  ],
  theme: {
    extend: {},
  },
  plugins: [
    require('tw-elements/dist/plugin')
  ],
}